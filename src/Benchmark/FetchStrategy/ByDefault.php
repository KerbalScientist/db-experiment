<?php

declare(strict_types=1);

namespace App\Benchmark\FetchStrategy;

use App\Benchmark\FetchStrategyInterface;
use PDO;
use PDOStatement;

class ByDefault implements FetchStrategyInterface
{
    private int $fetchStyle;
    private int $cursorOrientation;
    private int $offset;
    private ?int $limit;

    /**
     * @var mixed[]
     */
    private array $results = [];

    /**
     * ByDefault constructor.
     *
     * @param int      $fetchStyle
     * @param int      $cursorOrientation
     * @param int      $offset
     * @param int|null $limit
     */
    public function __construct(
        int $fetchStyle = PDO::FETCH_BOTH,
        int $cursorOrientation = PDO::FETCH_ORI_NEXT,
        int $offset = 0,
        ?int $limit = null
    ) {
        $this->fetchStyle = $fetchStyle;
        $this->cursorOrientation = $cursorOrientation;
        $this->offset = $offset;
        $this->limit = $limit;
    }

    /**
     * {@inheritdoc}
     */
    public function fetch(PDOStatement $statement): void
    {
        $this->results = [];
        $limit = $this->limit;
        while ((is_null($limit) || $limit--)
            && false !== ($result = $statement->fetch($this->fetchStyle, $this->cursorOrientation, $this->offset))) {
            $this->results[] = $result;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getResults()
    {
        return $this->results;
    }
}
