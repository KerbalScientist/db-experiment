<?php

declare(strict_types=1);

namespace App\Benchmark\FetchStrategy;

use App\Benchmark\FetchStrategyInterface;
use PDO;
use PDOStatement;

class Phalcon implements FetchStrategyInterface
{
    private int $fetchStyle;

    private ?int $offset;

    private ?int $limit;

    /**
     * @var mixed[]
     */
    private array $results = [];

    /**
     * ByDefault constructor.
     *
     * @param int      $fetchStyle
     * @param int|null $offset
     * @param int|null $limit
     */
    public function __construct(
        $fetchStyle = PDO::FETCH_BOTH,
        $offset = null,
        $limit = null
    ) {
        $this->fetchStyle = $fetchStyle;
        $this->offset = $offset;
        $this->limit = $limit;
    }

    /**
     * {@inheritdoc}
     */
    public function fetch(PDOStatement $statement): void
    {
        $this->results = [];
        $offset = 0;
        if (!is_null($this->offset)) {
            $offset = $this->offset;
        }
        $limit = $this->limit;
        $n = -1;
        --$offset;
        while ($n != $offset) {
            $statement->fetch();
            ++$n;
        }
        while ((is_null($limit) || $limit--)
            && false !== ($result = $statement->fetch($this->fetchStyle))) {
            $this->results[] = $result;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getResults()
    {
        return $this->results;
    }
}
