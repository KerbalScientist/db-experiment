<?php

declare(strict_types=1);

namespace App\Benchmark\FetchStrategy;

use App\Benchmark\FetchStrategyInterface;
use PDOStatement;

class Skip implements FetchStrategyInterface
{
    /**
     * {@inheritdoc}
     */
    public function fetch(PDOStatement $statement): void
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getResults()
    {
    }
}
