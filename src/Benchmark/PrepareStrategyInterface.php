<?php

declare(strict_types=1);

namespace App\Benchmark;

use PDO;
use PDOStatement;

interface PrepareStrategyInterface
{
    /**
     * @param PDO    $connection
     * @param string $query
     *
     * @return PDOStatement<mixed>
     */
    public function prepare(PDO $connection, string $query): PDOStatement;
}
