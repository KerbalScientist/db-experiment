<?php

declare(strict_types=1);

namespace App\Benchmark;

use PDOStatement;

interface FetchStrategyInterface
{
    /**
     * @param PDOStatement<mixed> $statement
     */
    public function fetch(PDOStatement $statement): void;

    /**
     * @return mixed
     */
    public function getResults();
}
