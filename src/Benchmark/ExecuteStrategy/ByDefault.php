<?php

declare(strict_types=1);

namespace App\Benchmark\ExecuteStrategy;

use App\Benchmark\ExecuteStrategyInterface;
use PDOStatement;

class ByDefault implements ExecuteStrategyInterface
{
    /**
     * @var mixed[]|null
     */
    private $inputParams;

    /**
     * ByDefault constructor.
     *
     * @param mixed[]|null $inputParams
     */
    public function __construct($inputParams = null)
    {
        $this->inputParams = $inputParams;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(PDOStatement $statement)
    {
        return $statement->execute($this->inputParams);
    }
}
