<?php

declare(strict_types=1);

namespace App\Benchmark\StatsParams;

class HistogramBin
{
    public int $index = 0;

    public float $start = 0;

    public float $end = 0.0;

    public int $count = 0;

    public float $density = 0.0;

    /**
     * HistogramBin constructor.
     *
     * @param mixed[]|null $params
     */
    public function __construct(?array $params = null)
    {
        if (!is_null($params)) {
            $this->index = $params['index'] ?? 0;
            $this->start = $params['start'] ?? 0.0;
            $this->end = $params['end'] ?? 0.0;
            $this->count = $params['count'] ?? 0;
            $this->density = $params['density'] ?? 0.0;
        }
    }
}
