<?php

declare(strict_types=1);

namespace App\Benchmark\StatsParams;

interface StatsParamVisitorInterface
{
    /**
     * @param NumericStatsParam $param
     */
    public function visitNumeric(NumericStatsParam $param): void;

    /**
     * @param Histogram $param
     */
    public function visitHistogram(Histogram $param): void;
}
