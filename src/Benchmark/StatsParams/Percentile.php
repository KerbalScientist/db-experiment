<?php

declare(strict_types=1);

namespace App\Benchmark\StatsParams;

class Percentile extends NumericStatsParam
{
    use StatsParamTrait {
        __construct as traitConstruct;
    }

    use StoresResultsTrait;

    /**
     * @var float
     */
    private float $percentileVal;

    public function __construct(string $paramName, float $percentileVal, ?string $name = null)
    {
        $this->percentileVal = $percentileVal;
        if (is_null($name) && 50 == $percentileVal) {
            $name = 'median'.ucfirst($paramName);
        } else {
            if (is_null($name)) {
                $name = 'percentile'
                    .str_replace(
                        '.',
                        '_',
                        (string) $percentileVal
                    )
                    .ucfirst($paramName);
            }
        }
        $this->traitConstruct($paramName, $name);
    }

    /**
     * {@inheritdoc}
     */
    public function getValue()
    {
        if (!count($this->results)) {
            return 0;
        }
        $targetCount = ceil(count($this->results) * ($this->percentileVal / 100));
        $this->results = $this->getSortedResults($this->paramName);
        foreach ($this->results as $result) {
            if (0 == --$targetCount) {
                return $result->{$this->paramName};
            }
        }

        return 0;
    }
}
