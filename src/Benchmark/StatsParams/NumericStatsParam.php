<?php

declare(strict_types=1);

namespace App\Benchmark\StatsParams;

use App\Benchmark\StatsParamInterface;

abstract class NumericStatsParam implements StatsParamInterface
{
    /**
     * @return float|int
     */
    abstract public function getValue();

    public function accept(StatsParamVisitorInterface $visitor): void
    {
        $visitor->visitNumeric($this);
    }
}
