<?php

declare(strict_types=1);

namespace App\Benchmark\StatsParams;

use App\Benchmark\IterationResult;

class Count extends NumericStatsParam
{
    /**
     * @var string
     */
    private string $name;

    /**
     * @var int
     */
    private int $count = 0;

    /**
     * Count constructor.
     *
     * @param string|null $name
     */
    public function __construct(?string $name = null)
    {
        if (is_null($name)) {
            $name = 'count';
        }
        $this->name = $name;
    }

    /**
     * {@inheritdoc}
     */
    public function getValue()
    {
        return $this->count;
    }

    /**
     * {@inheritdoc}
     */
    public function addResult(IterationResult $result): void
    {
        ++$this->count;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
