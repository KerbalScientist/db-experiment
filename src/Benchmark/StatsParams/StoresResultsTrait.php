<?php

declare(strict_types=1);

namespace App\Benchmark\StatsParams;

use App\Benchmark\IterationResult;

trait StoresResultsTrait
{
    /**
     * @var IterationResult[]
     */
    private array $results = [];

    /**
     * {@inheritdoc}
     */
    public function addResult(IterationResult $result): void
    {
        $this->results[] = $result;
    }

    /**
     * @param string $sortByParamName
     *
     * @return IterationResult[]
     */
    private function getSortedResults(string $sortByParamName): array
    {
        $results = $this->results;
        usort(
            $results,
            function ($result1, $result2) use ($sortByParamName) {
                $diff = $result1->$sortByParamName - $result2->$sortByParamName;
                if (0 === $diff) {
                    return 0;
                }

                return $diff > 0 ? 1 : -1;
            }
        );

        return $results;
    }
}
