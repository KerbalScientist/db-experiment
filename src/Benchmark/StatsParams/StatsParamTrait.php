<?php

declare(strict_types=1);

namespace App\Benchmark\StatsParams;

trait StatsParamTrait
{
    /**
     * @var string
     */
    private string $paramName;

    /**
     * @var string
     */
    private string $name;

    /**
     * StatsParamTrait constructor.
     *
     * @param string      $paramName
     * @param string|null $name
     */
    public function __construct(string $paramName, ?string $name = null)
    {
        $this->paramName = $paramName;
        if (is_null($name)) {
            $parts = explode('\\', get_called_class());
            $classBasename = (string) array_pop($parts);
            $name = lcfirst($classBasename).ucfirst($this->paramName);
        }
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
