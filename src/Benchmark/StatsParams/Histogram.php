<?php

declare(strict_types=1);

namespace App\Benchmark\StatsParams;

use App\Benchmark\IterationResult;
use App\Benchmark\StatsParamInterface;

class Histogram implements StatsParamInterface
{
    use StatsParamTrait {
        __construct as traitConstruct;
    }
    use StoresResultsTrait {
        addResult as traitAddResult;
    }

    const DEFAULT_BINS_COUNT = 15;

    /**
     * @var float
     */
    private ?float $binWidth;

    /**
     * @var bool
     */
    private bool $fillGaps = true;

    /**
     * @var float|int
     */
    private $minValue = INF;

    /**
     * @var float|int
     */
    private $maxValue = 0.0;

    /**
     * @var int
     */
    private int $defaultBinsCount = 15;

    /**
     * Histogram constructor.
     *
     * @param string      $paramName
     * @param string|null $name
     * @param float|null  $binWidth
     * @param bool        $fillGaps
     */
    public function __construct(string $paramName, ?string $name = null, ?float $binWidth = null, bool $fillGaps = true)
    {
        $this->binWidth = $binWidth;
        $this->fillGaps = $fillGaps;
        $this->traitConstruct($paramName, $name);
    }

    public function addResult(IterationResult $result): void
    {
        $this->maxValue = max($this->maxValue, $result->{$this->paramName});
        $this->minValue = min($this->minValue, $result->{$this->paramName});
        $this->traitAddResult($result);
    }

    /**
     * @return int
     */
    public function getDefaultBinsCount(): int
    {
        return $this->defaultBinsCount;
    }

    /**
     * @param int $defaultBinsCount
     */
    public function setDefaultBinsCount(int $defaultBinsCount): void
    {
        $this->defaultBinsCount = $defaultBinsCount;
    }

    /**
     * @return HistogramBin[]
     */
    public function getBins(): array
    {
        if (!count($this->results)) {
            return [];
        }
        $binWidth = $this->getBinWidth();
        $bins = [];
        $this->results = $this->getSortedResults($this->paramName);
        foreach ($this->results as $result) {
            $value = $result->{$this->paramName};
            $binIndex = (int) ceil(($value - $this->minValue) / $binWidth);
            if (!isset($bins[$binIndex])) {
                $bins[$binIndex] = new HistogramBin(
                    [
                        'index' => $binIndex,
                        'start' => $this->minValue + ($binWidth * $binIndex),
                        'end' => $this->minValue + ($binWidth * ($binIndex + 1)),
                        'count' => 0,
                    ]
                );
            }
            ++$bins[$binIndex]->count;
        }
        ksort($bins);
        foreach ($bins as $bin) {
            $bin->density = $bin->count / count($this->results);
        }
        if ($this->fillGaps) {
            $i = 0;
            foreach ($bins as $key => $bin) {
                while ($key != $i) {
                    $bins[$i] = new HistogramBin(
                        [
                            'index' => $i,
                            'start' => $this->minValue + ($binWidth * $i),
                            'end' => $this->minValue + ($binWidth * ($i + 1)),
                            'count' => 0,
                            'density' => 0.0,
                        ]
                    );
                    ++$i;
                }
                ++$i;
            }
            ksort($bins);
        }

        return $bins;
    }

    /**
     * @return float
     */
    public function getBinWidth(): float
    {
        return $this->binWidth ?? ($this->maxValue - $this->minValue) / $this->defaultBinsCount;
    }

    /**
     * @param float $binWidth
     */
    public function setBinWidth(float $binWidth): void
    {
        $this->binWidth = $binWidth;
    }

    /**
     * {@inheritdoc}
     */
    public function accept(StatsParamVisitorInterface $visitor): void
    {
        $visitor->visitHistogram($this);
    }
}
