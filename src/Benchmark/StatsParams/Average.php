<?php

declare(strict_types=1);

namespace App\Benchmark\StatsParams;

use App\Benchmark\IterationResult;

class Average extends NumericStatsParam
{
    use StatsParamTrait {
        __construct as traitConstruct;
    }

    /**
     * @var float
     */
    private float $sum = 0;

    /**
     * @var int
     */
    private int $count = 0;

    public function __construct(string $paramName, ?string $name = null)
    {
        if (is_null($name)) {
            $name = 'avg'.ucfirst($paramName);
        }
        $this->traitConstruct($paramName, $name);
    }

    /**
     * {@inheritdoc}
     */
    public function getValue()
    {
        return $this->sum / $this->count;
    }

    /**
     * {@inheritdoc}
     */
    public function addResult(IterationResult $result): void
    {
        $this->sum += $result->{$this->paramName};
        ++$this->count;
    }
}
