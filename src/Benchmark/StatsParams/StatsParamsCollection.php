<?php

declare(strict_types=1);

namespace App\Benchmark\StatsParams;

use App\Benchmark\IterationResult;
use App\Benchmark\StatsParamInterface;
use ArrayIterator;
use InvalidArgumentException;
use Iterator;
use IteratorAggregate;

class StatsParamsCollection implements IteratorAggregate, StatsParamInterface
{
    /**
     * @var string
     */
    private string $name;

    /**
     * @var StatsParamInterface[]
     */
    private array $params = [];

    /**
     * StatsParamsCollection constructor.
     *
     * @param string $name
     */
    public function __construct(?string $name = null)
    {
        if (is_null($name)) {
            $name = 'collection'.spl_object_id($this);
        }
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param StatsParamInterface $param
     *
     * @return self<StatsParamInterface>
     */
    public function addParam(StatsParamInterface $param): self
    {
        $this->params[$param->getName()] = $param;

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return Iterator<StatsParamInterface>
     */
    public function getIterator()
    {
        return new ArrayIterator($this->params);
    }

    public function addResult(IterationResult $result): void
    {
        foreach ($this->params as $param) {
            $param->addResult($result);
        }
    }

    /**
     * @param string $paramName
     *
     * @return StatsParamInterface
     *
     * @throws InvalidArgumentException
     */
    public function getByName(string $paramName): StatsParamInterface
    {
        if (!isset($this->params[$paramName])) {
            throw new InvalidArgumentException("Stats parameter '$paramName' not found.");
        }

        return $this->params[$paramName];
    }

    public function accept(StatsParamVisitorInterface $visitor): void
    {
        foreach ($this->params as $param) {
            $param->accept($visitor);
        }
    }
}
