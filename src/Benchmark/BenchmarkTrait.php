<?php

declare(strict_types=1);

namespace App\Benchmark;

use Exception;

trait BenchmarkTrait
{
    /**
     * @var int
     */
    private int $repeatCount = 50;

    /**
     * @var int
     */
    private int $warmupRepeatCount = 10;

    /**
     * @var int
     */
    private int $beforeRunSleepSeconds = 5;

    public function run(ResultSet $resultSet): ResultSet
    {
        sleep($this->beforeRunSleepSeconds);
        for ($i = 0; $i < $this->warmupRepeatCount + $this->repeatCount; ++$i) {
            $iterationResult = $this->runIteration($i - $this->warmupRepeatCount);
            if ($i > $this->warmupRepeatCount - 1) {
                $resultSet->addResult($iterationResult);
            }
        }

        return $resultSet;
    }

    /**
     * @param int $iterationIndex
     *
     * @return IterationResult
     *
     * @throws Exception
     */
    abstract protected function runIteration(int $iterationIndex): IterationResult;

    public function setRepeatCount(int $repeatCount): void
    {
        $this->repeatCount = $repeatCount;
    }

    public function setWarmupRepeatCount(int $warmupRepeatCount): void
    {
        $this->warmupRepeatCount = $warmupRepeatCount;
    }

    public function setBeforeRunSleepSeconds(int $beforeRunSleepSeconds): void
    {
        $this->beforeRunSleepSeconds = $beforeRunSleepSeconds;
    }
}
