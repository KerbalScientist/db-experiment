<?php

declare(strict_types=1);

namespace App\Benchmark;

use App\Benchmark\StatsParams\Histogram;
use App\Benchmark\StatsParams\HistogramBin;
use App\Benchmark\StatsParams\NumericStatsParam;
use App\Benchmark\StatsParams\StatsParamVisitorInterface;

class ArrayExportStatsParamVisitor implements StatsParamVisitorInterface
{
    /**
     * @var mixed[]
     */
    private array $result = [];

    /**
     * {@inheritdoc}
     */
    public function visitNumeric(NumericStatsParam $param): void
    {
        $this->result[$param->getName()] = $param->getValue();
    }

    /**
     * {@inheritdoc}
     */
    public function visitHistogram(Histogram $param): void
    {
        $this->result[$param->getName()] = array_map(
            function (HistogramBin $bin) {
                return get_object_vars($bin);
            },
            $param->getBins()
        );
    }

    /**
     * @return mixed[]
     */
    public function getResult()
    {
        return $this->result;
    }
}
