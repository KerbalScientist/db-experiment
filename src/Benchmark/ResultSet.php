<?php

declare(strict_types=1);

namespace App\Benchmark;

use App\Benchmark\StatsParams\Histogram;
use App\Benchmark\StatsParams\StatsParamsCollection;
use App\StudyInterface;

/**
 * Class ResultSet.
 */
class ResultSet
{
    /**
     * @var StudyInterface
     */
    private StudyInterface $study;

    /**
     * @var IterationResult[]
     */
    private array $results = [];

    /**
     * @var string
     */
    private string $displayName;

    /**
     * @var mixed[]
     */
    private array $metadata = [];

    /**
     * ResultSet constructor.
     *
     * @param StudyInterface $study
     * @param string         $displayName
     */
    public function __construct(StudyInterface $study, string $displayName)
    {
        $this->study = $study;
        $this->displayName = $displayName;
    }

    public function addResult(IterationResult $result): void
    {
        $this->results[] = $result;
    }

    /**
     * @param string $key
     * @param mixed  $value
     *
     * @return $this
     */
    public function addMetadata(string $key, $value): self
    {
        $this->metadata[$key] = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    /**
     * @return IterationResult[]
     */
    public function getResults(): array
    {
        return $this->results;
    }

    /**
     * @return mixed[]
     */
    public function getMetadata(): array
    {
        return $this->metadata;
    }

    /**
     * @return mixed[]
     */
    public function getSummaryAsArray(): array
    {
        $visitor = new ArrayExportStatsParamVisitor();
        $this->getStatsParams()->accept($visitor);
        $result = $visitor->getResult();

        return $result +
            [
                'metadata' => $this->metadata,
            ];
    }

    /**
     * @return StatsParamsCollection<StatsParamInterface>
     */
    public function getStatsParams(): StatsParamsCollection
    {
        $params = $this->study->createStatsParams();
        $this->fillStatsParams($params);

        return $params;
    }

    /**
     * @param StatsParamsCollection<StatsParamInterface> $params
     */
    public function fillStatsParams(StatsParamsCollection $params): void
    {
        foreach ($this->results as $result) {
            $params->addResult($result);
        }
    }

    /**
     * @param string     $paramName
     * @param float|null $binWidth
     * @param bool       $fillGaps
     *
     * @return Histogram
     */
    public function getHistogram(
        string $paramName,
        ?float $binWidth = null,
        bool $fillGaps = true
    ): Histogram {
        $hist = new Histogram($paramName, null, $binWidth, $fillGaps);
        foreach ($this->results as $result) {
            $hist->addResult($result);
        }

        return $hist;
    }
}
