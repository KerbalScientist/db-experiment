<?php

declare(strict_types=1);

namespace App\Benchmark;

use Exception;

interface BenchmarkInterface
{
    /**
     * @param ResultSet $resultSet
     *
     * @return ResultSet
     *
     * @throws Exception
     */
    public function run(ResultSet $resultSet): ResultSet;

    /**
     * @param int $repeatCount
     */
    public function setRepeatCount(int $repeatCount): void;

    /**
     * @param int $warmupRepeatCount
     */
    public function setWarmupRepeatCount(int $warmupRepeatCount): void;

    /**
     * @param int $beforeRunSleepSeconds
     */
    public function setBeforeRunSleepSeconds(int $beforeRunSleepSeconds): void;
}
