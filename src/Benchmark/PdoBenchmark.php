<?php

declare(strict_types=1);

namespace App\Benchmark;

use Exception;
use PDO;

class PdoBenchmark implements BenchmarkInterface
{
    use BenchmarkTrait {
        run as traitRun;
    }

    /**
     * @var PDO
     */
    private PDO $connection;

    /**
     * @var PrepareStrategyInterface
     */
    private PrepareStrategyInterface $prepareStrategy;

    /**
     * @var ExecuteStrategyInterface
     */
    private ExecuteStrategyInterface $executeStrategy;

    /**
     * @var FetchStrategyInterface
     */
    private FetchStrategyInterface $fetchStrategy;

    /**
     * @var string
     */
    private string $sql;

    /**
     * QueryTestBase constructor.
     *
     * @param PDO                      $connection
     * @param PrepareStrategyInterface $prepareStrategy
     * @param ExecuteStrategyInterface $executeStrategy
     * @param FetchStrategyInterface   $fetchStrategy
     */
    public function __construct(
        PDO $connection,
        PrepareStrategyInterface $prepareStrategy,
        ExecuteStrategyInterface $executeStrategy,
        FetchStrategyInterface $fetchStrategy
    ) {
        $this->connection = $connection;
        $this->prepareStrategy = $prepareStrategy;
        $this->executeStrategy = $executeStrategy;
        $this->fetchStrategy = $fetchStrategy;
    }

    /**
     * @param string $sql
     */
    public function setSql(string $sql): void
    {
        $this->sql = $sql;
    }

    public function run(ResultSet $resultSet): ResultSet
    {
        $resultSet->addMetadata('sql', $this->sql);

        return $this->traitRun($resultSet);
    }

    public function getRepeatCount(): int
    {
        return $this->repeatCount;
    }

    /**
     * @param int $iterationIndex
     *
     * @return IterationResult
     *
     * @throws Exception
     */
    protected function runIteration(int $iterationIndex): IterationResult
    {
        $iterationResult = new IterationResult();
        $iterationResult->add('index', $iterationIndex);
        $statement = $iterationResult->addMeasureTime(
            'prepareTime',
            [$this->prepareStrategy, 'prepare'],
            [$this->connection, $this->sql]
        );
        if (!$statement) {
            throw new Exception(
                'Error on statement prepare. Error info: ['
                .implode(', ', $this->connection->errorInfo()).']'
            );
        }
        $res = $iterationResult->addMeasureTime(
            'executeTime',
            [$this->executeStrategy, 'execute'],
            [$statement]
        );
        if (!$res) {
            throw new Exception(
                'Error on statement execute. Error info: ['
                .implode(', ', $statement->errorInfo()).']'
            );
        }
        $iterationResult->addMeasureTime(
            'fetchTime',
            [$this->fetchStrategy, 'fetch'],
            [$statement]
        );
        $iterationResult->add(
            'totalTime',
            $iterationResult->get('prepareTime')
            + $iterationResult->get('executeTime')
            + $iterationResult->get('fetchTime')
        );

        return $iterationResult;
    }
}
