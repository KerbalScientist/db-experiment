<?php

declare(strict_types=1);

namespace App\Benchmark;

use App\Benchmark\StatsParams\StatsParamVisitorInterface;

interface StatsParamInterface
{
    /**
     * @param IterationResult $result
     */
    public function addResult(IterationResult $result): void;

    public function getName(): string;

    /**
     * @param StatsParamVisitorInterface $visitor
     */
    public function accept(StatsParamVisitorInterface $visitor): void;
}
