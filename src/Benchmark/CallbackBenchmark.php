<?php

declare(strict_types=1);

namespace App\Benchmark;

class CallbackBenchmark implements BenchmarkInterface
{
    use BenchmarkTrait;

    /**
     * @var callable
     */
    private $callback;

    /**
     * CallbackBenchmark constructor.
     *
     * @param callable $callback
     */
    public function __construct(callable $callback)
    {
        $this->callback = $callback;
    }

    /**
     * {@inheritdoc}
     */
    protected function runIteration(int $iterationIndex): IterationResult
    {
        return call_user_func($this->callback, $iterationIndex);
    }
}
