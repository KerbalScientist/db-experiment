<?php

declare(strict_types=1);

namespace App\Benchmark\PrepareStrategy;

use App\Benchmark\PrepareStrategyInterface;
use PDO;
use PDOStatement;

class ByDefault implements PrepareStrategyInterface
{
    /**
     * @var mixed[]
     */
    private array $driverOptions;

    /**
     * ByDefault constructor.
     *
     * @param mixed[] $driverOptions
     */
    public function __construct(array $driverOptions = [])
    {
        $this->driverOptions = $driverOptions;
    }

    /**
     * @param PDO    $connection
     * @param string $query
     *
     * @return PDOStatement<mixed>
     */
    public function prepare(PDO $connection, string $query): PDOStatement
    {
        return $connection->prepare($query, $this->driverOptions);
    }
}
