<?php

declare(strict_types=1);

namespace App\Benchmark;

use PDOStatement;

interface ExecuteStrategyInterface
{
    /**
     * @param PDOStatement<mixed> $statement
     *
     * @return bool
     */
    public function execute(PDOStatement $statement);
}
