<?php

declare(strict_types=1);

namespace App\Benchmark;

use InvalidArgumentException;

/**
 * Class Result.
 */
class IterationResult
{
    /**
     * @var mixed[]
     */
    private array $params = [];

    /**
     * @param string $name
     *
     * @return mixed
     *
     * @throws InvalidArgumentException
     */
    public function __get(string $name)
    {
        return $this->get($name);
    }

    /**
     * @param string $name
     *
     * @return mixed
     *
     * @throws InvalidArgumentException
     */
    public function get(string $name)
    {
        if (!isset($this->params[$name])) {
            throw new InvalidArgumentException("Parameter '$name' is not defined.");
        }

        return $this->params[$name];
    }

    /**
     * @param string $name
     * @param mixed  $value
     */
    public function add(string $name, $value): void
    {
        $this->params[$name] = $value;
    }

    /**
     * @param string   $paramName
     * @param callable $callback
     * @param mixed[]  $callbackArgs
     *
     * @return mixed
     */
    public function addMeasureTime(
        string $paramName,
        callable $callback,
        array $callbackArgs = []
    ) {
        $start = microtime(true);
        $result = call_user_func_array($callback, $callbackArgs);
        $this->add($paramName, microtime(true) - $start);

        return $result;
    }
}
