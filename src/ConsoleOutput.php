<?php

declare(strict_types=1);

namespace App;

use App\Benchmark\ResultSet;
use App\Benchmark\StatsParamInterface;
use App\Benchmark\StatsParams\Histogram;
use App\Benchmark\StatsParams\StatsParamsCollection;
use InvalidArgumentException;

class ConsoleOutput
{
    /**
     * @param ResultSet[] $resultSets
     */
    public function printSummary(array $resultSets): void
    {
        echo "\nResults\n\n";
        foreach ($resultSets as $key => $resultSet) {
            echo "{$resultSet->getDisplayName()}:\n"
                .json_encode($resultSet->getSummaryAsArray(), JSON_PRETTY_PRINT)."\n\n";
        }
    }

    /**
     * @param ResultSet[]                                $resultSets
     * @param StatsParamsCollection<StatsParamInterface> $statsParamsCollection
     */
    public function printSummaryTable(
        array $resultSets,
        StatsParamsCollection $statsParamsCollection
    ): void {
        echo "\nResults\n\n";
        $colsHeader = [];
        foreach ($resultSets as $resultSet) {
            $colsHeader[] = $resultSet->getDisplayName();
        }
        $rowsHeader = [];
        foreach ($statsParamsCollection as $param) {
            $rowsHeader[] = $param->getName();
        }
        $visitor = new TableExportNumericStatsParamVisitor();
        $visitor->setConvertValueCallback(
            function ($value) {
                if (is_float($value)) {
                    return sprintf('%.3e', $value);
                }

                return (string) $value;
            }
        );
        foreach ($resultSets as $key => $resultSet) {
            $setParams = $resultSet->getStatsParams();
            /**
             * @var StatsParamInterface $param
             */
            foreach ($statsParamsCollection as $param) {
                $setParams
                    ->getByName($param->getName())
                    ->accept($visitor);
            }
        }
        $data = $visitor->getData();
        $table[] = array_merge([''], $colsHeader);
        foreach ($statsParamsCollection as $param) {
            $table[] = array_merge(
                [$param->getName()],
                $data[$param->getName()]
            );
        }
        $this->printTable($table);
    }

    /**
     * @param array[] $table
     * @param int     $paddingWidth
     */
    private function printTable(array $table, int $paddingWidth = 1): void
    {
        $crossingChar = '+';
        $hLineChar = '-';
        $vLineChar = '|';
        if (!$table) {
            return;
        }
        if (!is_array($table[0])) {
            throw new InvalidArgumentException('Table must be a two-dimensional array.');
        }
        $colsWidth = [];
        foreach ($table as $row) {
            foreach ($row as $colKey => $cell) {
                if (!isset($colsWidth[$colKey])) {
                    $colsWidth[$colKey] = 0;
                }
                $colsWidth[$colKey] = max($colsWidth[$colKey], strlen($cell));
            }
        }
        $hLine = '';
        foreach ($colsWidth as $width) {
            $hLine .= str_pad(
                '+',
                $width + $paddingWidth * 2 + 1,
                $hLineChar
            );
        }
        $hLine .= $crossingChar;
        echo $hLine.PHP_EOL;
        $firstRow = true;
        foreach ($table as $row) {
            foreach ($row as $colKey => $cell) {
                echo $vLineChar;
                echo str_pad('', $paddingWidth);
                echo str_pad($cell, $colsWidth[$colKey], ' ', STR_PAD_LEFT);
                echo str_pad('', $paddingWidth);
            }
            echo $vLineChar.PHP_EOL;
            if ($firstRow) {
                $firstRow = false;
                echo $hLine.PHP_EOL;
            }
        }
        echo $hLine.PHP_EOL;
    }

    /**
     * @param ResultSet[] $resultSets
     * @param string      $paramName
     * @param float|null  $binWidth
     * @param int         $barWidth
     */
    public function printHistograms(
        array $resultSets,
        string $paramName,
        ?float $binWidth = null,
        int $barWidth = 50
    ): void {
        echo "\nHistogram ($paramName)\n\n";
        $histograms = [];
        $autoBinWidth = INF;
        foreach ($resultSets as $key => $resultSet) {
            $histogram = $resultSet->getHistogram($paramName);
            $histograms[$key] = $histogram;
            if (is_null($binWidth)) {
                $autoBinWidth = min($autoBinWidth, $histogram->getBinWidth());
            }
        }
        if (is_null($binWidth)) {
            $binWidth = $autoBinWidth;
        }
        foreach ($resultSets as $key => $resultSet) {
            $histograms[$key]->setBinWidth($binWidth);
            echo "{$resultSet->getDisplayName()}:\n"
                .$this->convertHistogramToText($histograms[$key], $barWidth)."\n";
        }
    }

    private function convertHistogramToText(Histogram $histogram, int $barWidth = 50): string
    {
        $result = '';
        $data = $histogram->getBins();
        foreach ($data as $bin) {
            $binWidth = (int) round($bin->density * $barWidth);
            $legend = sprintf('(%.4g to %.4g) - %.2f%%', $bin->start, $bin->end, $bin->density * 100);
            $result .= str_pad('', $binWidth, '=').">  $legend\n";
        }

        return $result;
    }
}
