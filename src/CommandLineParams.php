<?php

declare(strict_types=1);

namespace App;

use InvalidArgumentException;

class CommandLineParams
{
    const OPTION_PREFIX = '--';

    const VALUE_SEPARATOR = '=';

    /**
     * @var mixed[]
     */
    private array $options;

    /**
     * @var mixed[]
     */
    private array $args;

    /**
     * @var mixed[]
     */
    private array $resultOptions = [];

    /**
     * @var mixed[]
     */
    private array $resultArgs = [];

    /**
     * CommandLineParams constructor.
     *
     * @param mixed[] $options
     * @param mixed[] $args
     */
    public function __construct(array $options, array $args)
    {
        $this->options = $options;
        $this->args = $args;
    }

    /**
     * @param string[] $argv
     */
    public function parse(array $argv): void
    {
        array_shift($argv);
        reset($this->args);
        foreach ($argv as $arg) {
            if (self::OPTION_PREFIX !== substr($arg, 0, strlen(self::OPTION_PREFIX))) {
                $this->resultArgs[] = $arg;
                $currentArgKey = key($this->args);
                if (is_null($currentArgKey)) {
                    throw new InvalidArgumentException("Unknown argument '$arg'.");
                }
                $this->resultArgs[$currentArgKey] = $arg;
                next($this->args);
            }
            $arg = substr($arg, strlen(self::OPTION_PREFIX));
            if (false !== strpos($arg, self::VALUE_SEPARATOR)) {
                [$key, $value] = explode(self::VALUE_SEPARATOR, $arg);
                $this->resultOptions[$key] = $value;
            } else {
                $this->resultOptions[$arg] = true;
            }
        }
        $this->resultArgs += $this->args;
        $this->resultOptions += $this->options;
    }

    /**
     * @param string $name
     *
     * @return mixed|null
     */
    public function getResultOption(string $name)
    {
        return $this->resultOptions[$name] ?? null;
    }

    /**
     * @param string $name
     *
     * @return mixed|null
     */
    public function getResultArg(string $name)
    {
        return $this->resultArgs[$name] ?? null;
    }
}
