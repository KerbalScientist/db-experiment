<?php

declare(strict_types=1);

namespace App;

use App\Benchmark\ResultSet;
use App\Benchmark\StatsParamInterface;
use App\Benchmark\StatsParams\StatsParamsCollection;
use Exception;

interface StudyInterface
{
    /**
     * @return ResultSet[]
     *
     * @throws Exception
     */
    public function runAll(): array;

    /**
     * @return StatsParamsCollection<StatsParamInterface>
     */
    public function createStatsParams(): StatsParamsCollection;
}
