<?php

declare(strict_types=1);

namespace App;

use App\Benchmark\StatsParams\Histogram;
use App\Benchmark\StatsParams\NumericStatsParam;
use App\Benchmark\StatsParams\StatsParamVisitorInterface;

class TableExportNumericStatsParamVisitor implements StatsParamVisitorInterface
{
    /**
     * @var array[]
     */
    private array $result = [];

    /**
     * @var ?callable
     */
    private $convertValueCallback;

    /**
     * {@inheritdoc}
     */
    public function visitNumeric(NumericStatsParam $param): void
    {
        if (!isset($this->result[$param->getName()])) {
            $this->result[$param->getName()] = [];
        }
        if ($this->convertValueCallback) {
            $this->result[$param->getName()][] = ($this->convertValueCallback)($param->getValue());
        } else {
            $this->result[$param->getName()][] = $param->getValue();
        }
    }

    /**
     * @param callable $convertValueCallback
     */
    public function setConvertValueCallback(callable $convertValueCallback): void
    {
        $this->convertValueCallback = $convertValueCallback;
    }

    /**
     * {@inheritdoc}
     */
    public function visitHistogram(Histogram $param): void
    {
    }

    /**
     * @return array[]
     */
    public function getData(): array
    {
        return $this->result;
    }
}
