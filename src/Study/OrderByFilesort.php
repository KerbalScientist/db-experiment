<?php

declare(strict_types=1);

namespace App\Study;

use App\Benchmark\ExecuteStrategy\ByDefault as ExecuteStrategy;
use App\Benchmark\FetchStrategy\Skip as SkipFetchStrategy;
use App\Benchmark\PdoBenchmark;
use App\Benchmark\PrepareStrategy\ByDefault as PrepareStrategy;
use App\Benchmark\ResultSet;
use Exception;

class OrderByFilesort extends PdoStudyBase
{
    const MIN_BUFFER_SIZE = 32768;
    const DEFAULT_BUFFER_SIZE = 262144;
    const X4_BUFFER_SIZE = self::DEFAULT_BUFFER_SIZE * 4;
    const X16_BUFFER_SIZE = self::DEFAULT_BUFFER_SIZE * 16;
    const X32_BUFFER_SIZE = self::DEFAULT_BUFFER_SIZE * 32;
    const X10024_BUFFER_SIZE = self::DEFAULT_BUFFER_SIZE * 10024;
    const MAX_BUFFER_SIZE = 18446744073709551615;

    private int $repeatCount = 100;
    private int $warmupRepeatCount = 50;
    private int $beforeRunSleepSeconds = 20;

    /**
     * {@inheritdoc}
     */
    public function runAll(): array
    {
        return [
//            $this->runBench('MIN_BUFFER_SIZE', self::MIN_BUFFER_SIZE),
            $this->runBench('X4_BUFFER_SIZE', self::X4_BUFFER_SIZE),
//            $this->runBench('X16_BUFFER_SIZE', self::X16_BUFFER_SIZE),
//            $this->runBench('X32_BUFFER_SIZE', self::X32_BUFFER_SIZE),
            $this->runBench('X10024_BUFFER_SIZE', self::X10024_BUFFER_SIZE),
            $this->runBench('DEFAULT_BUFFER_SIZE', self::DEFAULT_BUFFER_SIZE),
//            $this->runBench('MAX_BUFFER_SIZE', self::MAX_BUFFER_SIZE),
        ];
    }

    /**
     * @param string $name
     * @param int    $sortBufferSize
     *
     * @return ResultSet
     *
     * @throws Exception
     */
    private function runBench(string $name, int $sortBufferSize): ResultSet
    {
        $execResult = $this->dbConnection->exec("SET LOCAL sort_buffer_size=$sortBufferSize");
        if (false === $execResult) {
            throw new Exception('Cannot set sort_buffer_size.');
        }
        $benchmark = new PdoBenchmark(
            $this->dbConnection,
            new PrepareStrategy(),
            new ExecuteStrategy(),
            new SkipFetchStrategy()
        );
        $benchmark->setRepeatCount($this->repeatCount);
        $benchmark->setWarmupRepeatCount($this->warmupRepeatCount);
        $benchmark->setBeforeRunSleepSeconds($this->beforeRunSleepSeconds);
        $result = new ResultSet($this, $name);
        $benchmark->setSql(
            'SELECT p.* FROM s_products p
                    LEFT JOIN product_warehouse pw ON pw.product_id = p.id
                    WHERE (pw.amount > 0)
                    GROUP BY p.id
                    ORDER BY MAX(pw.amount)'
        );

        return $benchmark->run($result);
    }
}
