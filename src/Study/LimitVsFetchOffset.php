<?php

declare(strict_types=1);

namespace App\Study;

use App\Benchmark\ExecuteStrategy\ByDefault as ExecuteStrategy;
use App\Benchmark\FetchStrategy\ByDefault as FetchStrategy;
use App\Benchmark\FetchStrategy\Phalcon as PhalconFetchStrategy;
use App\Benchmark\PdoBenchmark;
use App\Benchmark\PrepareStrategy\ByDefault as PrepareStrategy;
use App\Benchmark\ResultSet;
use Exception;
use PDO;

class LimitVsFetchOffset extends PdoStudyBase
{
    private int $offset = 50000;
    private int $limit = 64;
    private int $repeatCount = 100;
    private int $warmupRepeatCount = 50;

    /**
     * @var int[]
     */
    private array $dataLimitIds = [];

    /**
     * @var int[]
     */
    private array $dataFetchIds = [];

    public function testDataEquals(): bool
    {
        return $this->dataFetchIds === $this->dataLimitIds;
    }

    public function runAll(): array
    {
        return [
            $this->countBench(),
            $this->limitBench(),
            $this->fetchOffsetBench(),
        ];
    }

    /**
     * @return ResultSet
     *
     * @throws Exception
     */
    public function countBench(): ResultSet
    {
        $this->dbConnection->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
        $benchmark = new PdoBenchmark(
            $this->dbConnection,
            new PrepareStrategy(),
            new ExecuteStrategy(),
            $fetchStrategy = new FetchStrategy()
        );
        $benchmark->setRepeatCount($this->repeatCount);
        $benchmark->setWarmupRepeatCount($this->warmupRepeatCount);
        $result = new ResultSet($this, 'Count');
        $benchmark->setSql(
            '
            SELECT SQL_NO_CACHE count(*) FROM s_products
        '
        );
        $benchmark->run($result);

        return $result;
    }

    /**
     * @return ResultSet
     *
     * @throws Exception
     */
    public function limitBench(): ResultSet
    {
        $this->dbConnection->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
        $benchmark = new PdoBenchmark(
            $this->dbConnection,
            new PrepareStrategy(),
            new ExecuteStrategy(),
            $fetchStrategy = new FetchStrategy()
        );
        $benchmark->setRepeatCount($this->repeatCount);
        $benchmark->setWarmupRepeatCount($this->warmupRepeatCount);
        $result = new ResultSet($this, 'Limit');
        $benchmark->setSql(
            "SELECT SQL_NO_CACHE id FROM s_products
                    ORDER BY id
                    LIMIT $this->offset, $this->limit"
        );
        $benchmark->run($result);
        $this->dataLimitIds = array_column($fetchStrategy->getResults(), 'id');

        return $result;
    }

    /**
     * @return ResultSet
     *
     * @throws Exception
     */
    public function fetchOffsetBench(): ResultSet
    {
        $this->dbConnection->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, false);
        $benchmark = new PdoBenchmark(
            $this->dbConnection,
            new PrepareStrategy([PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]),
            new ExecuteStrategy(),
            $fetchStrategy = new PhalconFetchStrategy(PDO::FETCH_ASSOC, $this->offset, $this->limit)
        );
        $benchmark->setRepeatCount($this->repeatCount);
        $benchmark->setWarmupRepeatCount($this->warmupRepeatCount);
        $result = new ResultSet($this, 'Data seek');
        $benchmark->setSql(
            '
            SELECT SQL_NO_CACHE id FROM s_products ORDER BY id
        '
        );
        $benchmark->run($result);
        $this->dataFetchIds = array_column($fetchStrategy->getResults(), 'id');

        return $result;
    }
}
