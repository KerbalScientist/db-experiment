<?php

declare(strict_types=1);

namespace App\Study;

use App\Benchmark\ExecuteStrategy\ByDefault as ExecuteStrategy;
use App\Benchmark\FetchStrategy\Skip as SkipFetchStrategy;
use App\Benchmark\PdoBenchmark;
use App\Benchmark\PrepareStrategy\ByDefault as PrepareStrategy;
use App\Benchmark\ResultSet;

class JoinImpact extends PdoStudyBase
{
    private int $warmupRepeatCount = 20;
    private int $repeatCount = 100;

    /**
     * {@inheritdoc}
     */
    public function runAll(): array
    {
        $queries = [
            'No joins' => '
                SELECT SQL_NO_CACHE so.feature_id
                    FROM s_options so
                LIMIT 100000, 100
            ',
            '1 join' => '
                SELECT SQL_NO_CACHE so.feature_id
                    FROM s_variants v
                    LEFT JOIN s_options so on v.product_id = so.product_id
                LIMIT 100000, 100
            ',
            '2 joins' => '
                SELECT SQL_NO_CACHE so.feature_id
                    FROM s_variants v
                    LEFT JOIN s_products p on p.id = v.product_id
                    LEFT JOIN s_options so on p.id = so.product_id
                LIMIT 100000, 100
            ',
        ];

        $benchmark = new PdoBenchmark(
            $this->dbConnection,
            new PrepareStrategy(),
            new ExecuteStrategy(),
            new SkipFetchStrategy()
        );
        $benchmark->setRepeatCount($this->repeatCount);
        $benchmark->setWarmupRepeatCount($this->warmupRepeatCount);
        $result = [];
        foreach ($queries as $name => $sql) {
            $resultSet = new ResultSet($this, $name);
            $benchmark->setSql($sql);
            $result[$name] = $benchmark->run($resultSet);
        }

        return $result;
    }
}
