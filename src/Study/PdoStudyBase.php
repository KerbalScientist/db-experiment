<?php

declare(strict_types=1);

namespace App\Study;

use App\Benchmark\StatsParams\Average;
use App\Benchmark\StatsParams\Count;
use App\Benchmark\StatsParams\Percentile;
use App\Benchmark\StatsParams\StatsParamsCollection;
use App\ServiceLocator;
use App\StudyInterface;
use PDO;

abstract class PdoStudyBase implements StudyInterface
{
    /**
     * @var PDO
     */
    protected PDO $dbConnection;

    /**
     * BenchmarkBase constructor.
     *
     * @param ServiceLocator $serviceLocator
     */
    public function __construct(ServiceLocator $serviceLocator)
    {
        $this->dbConnection = $serviceLocator->getDbConnection();
    }

    public function createStatsParams(): StatsParamsCollection
    {
        $params = new StatsParamsCollection();
        $params->addParam(new Count('repeatCount'))
            ->addParam(new Average('totalTime'))
            ->addParam(new Percentile('totalTime', 50.0))
            ->addParam(new Percentile('totalTime', 95.0))
            ->addParam(new Percentile('totalTime', 99.0))
            ->addParam(new Average('prepareTime'))
            ->addParam(new Average('executeTime'))
            ->addParam(new Average('fetchTime'));

        return $params;
    }
}
