<?php

declare(strict_types=1);

namespace App;

use InvalidArgumentException;
use Throwable;

class Application
{
    const OPTION_STUDY_CLASS = 'study-class';
    const OPTION_SHOW_HISTOGRAMS = 'show-histograms';
    const OPTION_HISTOGRAM_PARAM_NAME = 'hist-param-name';

    private ServiceLocator $serviceLocator;

    public function __construct(?ServiceLocator $serviceLocator = null)
    {
        $this->serviceLocator = $serviceLocator ?? new ServiceLocator();
    }

    /**
     * @param string[]|null $argv
     */
    public function run(?array $argv = null): void
    {
        try {
            if (is_null($argv)) {
                $argv = $GLOBALS['argv'];
            }
            $params = new CommandLineParams(
                [
                    self::OPTION_SHOW_HISTOGRAMS => false,
                    self::OPTION_HISTOGRAM_PARAM_NAME => 'totalTime',
                ],
                [
                    self::OPTION_STUDY_CLASS => null,
                ]
            );
            $params->parse($argv);
            $class = $params->getResultArg(self::OPTION_STUDY_CLASS);
            if (!class_exists($class)) {
                throw new InvalidArgumentException("Class $class not found.");
            }

            /**
             * @var StudyInterface $study
             */
            $study = new $class($this->serviceLocator);
            $resultSets = $study->runAll();

            $output = $this->serviceLocator->getConsoleOutput();
            $output->printSummaryTable($resultSets, $study->createStatsParams());
            if ($params->getResultOption(self::OPTION_SHOW_HISTOGRAMS)) {
                $output->printHistograms(
                    $resultSets,
                    (string) $params->getResultOption(self::OPTION_HISTOGRAM_PARAM_NAME)
                );
            }
        } catch (Throwable $e) {
            echo $e->__toString();
        }
    }
}
