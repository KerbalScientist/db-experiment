<?php

declare(strict_types=1);

namespace App;

use PDO;

class ServiceLocator
{
    /**
     * @var PDO
     */
    private PDO $dbConnection;
    /**
     * @var ConsoleOutput
     */
    private ConsoleOutput $consoleOutput;

    public function getDbConnection(): PDO
    {
        if (empty($this->dbConnection)) {
            $this->dbConnection = new PDO(
                (string) getenv('MARIADB_PDO_DSN'),
                (string) getenv('MARIADB_USERNAME'),
                (string) getenv('MARIADB_PASSWORD'),
                [
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => false,
                ]
            );
        }

        return $this->dbConnection;
    }

    public function getConsoleOutput(): ConsoleOutput
    {
        if (empty($this->consoleOutput)) {
            $this->consoleOutput = new ConsoleOutput();
        }

        return $this->consoleOutput;
    }
}
