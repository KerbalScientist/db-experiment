# DB Benchmark utility
Runs DB query related code multiple times and shows timing statistics.

## Environment
- Any Linux distro (Ubuntu >= 16.04 recommended)
- Docker >= 19.03
- Docker Compose >= 1.24
- GNU Make utility

## Installation
`make install`

## Usage
Show joins impact on query performance.

`make run -- '\\App\\Study\\JoinImpact'`  
or  
`make run -- '\\App\\Study\\JoinImpact' --show-histograms` 

Show sort_buffer_size MySQL/MariaDB system variable impact on sorting performance.  

`make run -- '\\App\\Study\\OrderByFilesort'`

Show the inferiority of "use unbuffered queries, SELECT without LIMIT, then seek" approach compared to traditional COUNT+LIMIT approach.

`make run -- '\\App\\Study\\LimitVsFetchOffset'`

## Uninstallation

`make clean`

## Run development tasks

Run tests (only code style and static analysis for now):

`make test`

Fix code style:

`make fix-phpcs`
