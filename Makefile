#!/usr/bin/make
# Makefile readme (ru): <http://linux.yaroslavl.ru/docs/prog/gnu_make_3-79_russian_manual.html>
# Makefile readme (en): <https://www.gnu.org/software/make/manual/html_node/index.html#SEC_Contents>

SHELL = /bin/sh

CONTAINER_NAME := app
CONTAINER_SHELL = /bin/sh
ARGS = ""

docker_bin := $(shell command -v docker 2> /dev/null)
docker_compose_bin := $(shell command -v docker-compose 2> /dev/null)


ifeq "$(REGISTRY_HOST)" "registry.gitlab.com"
	docker_login_hint ?= "\n\
	**************************************************************************************\n\
	* Make your own auth token here: <https://gitlab.com/profile/personal_access_tokens> *\n\
	**************************************************************************************\n"
endif

ifeq (run,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif

.PHONY : help login install clean \
         run up down restart shell root \
         test test-phpstan test-phpcs fix-phpcs
.DEFAULT_GOAL := help

# This will output the help for each task. thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
help: ## Show this help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)
	@echo "\n  It's no help"

# --- [ Application ] -------------------------------------------------------------------------------------------------

login: ## Log in to a remote Docker registry
	@echo $(docker_login_hint)
	$(docker_bin) login $(REGISTRY_HOST)

run: up ## Run application
	$(docker_compose_bin) exec "$(CONTAINER_NAME)" composer run-app -- $(RUN_ARGS)

up: ## Start all containers (in background) for development
	$(docker_compose_bin) up --no-recreate -d

down: ## Stop all started for development containers
	$(docker_compose_bin) down

restart: up ## Restart all started for development containers
	$(docker_compose_bin) restart

shell: up ## Start shell into application container
	$(docker_compose_bin) exec "$(CONTAINER_NAME)" "$(CONTAINER_SHELL)"

root: up ## Start shell into application container with root
	$(docker_compose_bin) exec -u root "$(CONTAINER_NAME)" "$(CONTAINER_SHELL)"

install: up ## Install application dependencies into application container
	$(docker_compose_bin) exec "$(CONTAINER_NAME)" composer install --no-interaction --ansi --no-suggest
	sleep 60 # Wait for db container to load fixtures.
# @todo Do it reliable.

init: install ## Make full application initialization (install, seed, build assets, etc)

clean: ## Remove containers
	-$(docker_compose_bin) down -v
	-$(docker_compose_bin) rm -f -v

# --- [ Development tasks ] -------------------------------------------------------------------------------------------

test: test-phpstan test-phpcs ## Run full set of tests

test-phpstan: up ## Run PHPStan
	$(docker_compose_bin) exec "$(CONTAINER_NAME)" composer test:phpstan

test-phpcs: ## Run ECS
	$(docker_compose_bin) exec "$(CONTAINER_NAME)" composer test:phpcs

fix-phpcs: ## Run ECS fix
	$(docker_compose_bin) exec "$(CONTAINER_NAME)" composer fix:phpcs
